package RabbitmqRoute

/**
数据请求
*/
type dataRequest struct {
	Path   string                 `json:"path"`   //请求路由
	Data   string                 `json:"data"`   //请求数据
	Header map[string]interface{} `json:"header"` //请求头
}

func NewDataRequestPath(path string, data string) *dataRequest {
	return &dataRequest{Path: path, Data: data, Header:make(map[string]interface{})}
}

func NewDataRequest(path string, data string, header map[string]interface{}) *dataRequest {
	if header==nil{
		header = make(map[string]interface{})
	}
	return &dataRequest{Path: path, Data: data, Header: header}
}

func (d *dataRequest) GetPath() string {
	return d.Path
}

func (d *dataRequest) SetPath(path string) {
	d.Path = path
}

func (d *dataRequest) GetData() string {
	return d.Data
}

func (d *dataRequest) SetData(data string) {
	d.Data = data
}

func (d *dataRequest) GetHeader() map[string]interface{} {
	return d.Header
}
func (d *dataRequest) SetHeader(header map[string]interface{}) {
	d.Header = header
}
