package RabbitmqRoute

import (
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"errors"
	"fmt"
	"strings"
)

//结构体二进制处理

/**
数据协议封装
*/
func dataBinary(data interface{}) ([]byte, error) {
	buf := new(bytes.Buffer)
	if err := gob.NewEncoder(buf).Encode(data); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}



/**
数据解析
*/
func DataEngineParse(base64Str string, out interface{}) error {
	base64Bytes, err := base64.StdEncoding.DecodeString(base64Str)
	if err != nil {
		return errors.New(fmt.Sprintf("base64 decode error :%s", err.Error()))
	}
	var buf bytes.Buffer
	buf.Write(base64Bytes)
	ad := buf.String()
	buf.Reset()
	err = gob.NewDecoder(strings.NewReader(ad)).Decode(out)
	return nil
}