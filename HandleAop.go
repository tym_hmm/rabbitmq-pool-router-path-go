package RabbitmqRoute

/**
切片路由

 */
type HandlerAop interface {

	/**
	路由前处理数据
	 */
	BeforeHandle(c *TaskContext)

}
