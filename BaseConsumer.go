package RabbitmqRoute

import kelleyRabbimqPool "gitee.com/tym_hmm/rabbitmq-pool-go"

type BaseConsumer struct {
	TaskName     string
	ExchangeName string
	ExChangeType string
	VirtualHosts string
	Queue        string
	RouteKey     string
	IsTry        bool  //是否重试
	MaxReTry     int32 //最大重式次数
	IsAutoAck    bool  //是否自动确认
}

/**
发送消息到rmq
*/
func (a *BaseConsumer) Receive(rabbitMqPool *kelleyRabbimqPool.RabbitPool, successEvent func(data []byte, header map[string]interface{}, retryClient kelleyRabbimqPool.RetryClientInterface) bool, failEvent func(code int, err error)) {
	normal := &kelleyRabbimqPool.ConsumeReceive{
		ExchangeName: a.ExchangeName,
		ExchangeType: a.ExChangeType,
		Route:        a.RouteKey,
		QueueName:    a.Queue,
		IsTry:        a.IsTry,
		MaxReTry:     a.MaxReTry,
		IsAutoAck:    a.IsAutoAck,
		EventFail: func(code int, e error, data []byte) {
			failEvent(code, e)
			return
		},
		EventSuccess: func(data []byte, header map[string]interface{}, retryClient kelleyRabbimqPool.RetryClientInterface) bool {
			return successEvent(data, header, retryClient)
		},
	}
	rabbitMqPool.RegisterConsumeReceive(normal)
	err := rabbitMqPool.RunConsume()
	if err != nil {
		failEvent(CODE_RABBITMQ_RECEIVED_ERROR, err)
		return
	}

}
