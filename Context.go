package RabbitmqRoute

import (
	kelleyRabbimqPool "gitee.com/tym_hmm/rabbitmq-pool-go"
	"math"
	"strings"
	"sync"
)


const abortIndex int8 = math.MaxInt8 / 2

/**
数据中心上下文
*/
type TaskContext struct {
	Request     *dataRequest
	consumer    *dataCenterConsumer
	RetryClient kelleyRabbimqPool.RetryClientInterface
	isReTry     bool //是否重试
	engine      *TaskEngine
	handlers    HandlersChain
	index       int8
	mu          sync.RWMutex
}

func (c *TaskContext) reset() {
	c.handlers = nil
	c.index = -1
	c.isReTry = false
}

func (c *TaskContext) SetIsTry(isTry bool) {
	c.isReTry = isTry
}

func (c *TaskContext) IsAborted() bool {
	return c.index >= abortIndex
}

func (c *TaskContext) Next() {
	c.index++
	for c.index < int8(len(c.handlers)) {
		c.handlers[c.index](c)
		c.index++
	}
}

/**
处理引擎
*/
type TaskEngine struct {
	handlerAop HandlerAop
	Router
	trees *RouterTree
	pool  sync.Pool
}

/**
初始化路由引擎
*/
func NewDataCenterEngine() *TaskEngine {
	engine := &TaskEngine{
		Router: Router{basePath: "/"},
		trees: &RouterTree{root: &routerNode{
			path:        "/",
			children:    nil,
			httpHandler: nil,
			index:       0,
		}},
	}
	engine.Router.engine = engine
	engine.pool.New = func() interface{} {
		return &TaskContext{engine: engine}
	}
	return engine
}

func (dce *TaskEngine) routeHandle(c *TaskContext) {
	reqPath := c.Request.Path

	routeTree := dce.trees
	pathRoot := dce.trees.getRoot()
	pathSlice := strings.Split(reqPath, "/")
	pathSlice = sliceFilterString(pathSlice)

	if pathRoot != nil {
		childNode := routeTree.getChildNode(reqPath, pathRoot)
		if childNode != nil {
			if childNode.httpHandler != nil {
				if dce.handlerAop != nil {
					a := []HandleFunc{}
					a = append(a, dce.handlerAop.BeforeHandle)
					c.handlers = insertHandleSliceCopy(childNode.httpHandler, a, 0)
				} else {
					c.handlers = childNode.httpHandler
				}
				c.Next()
				return
			}
			return
		} else {
			//fmt.Println(c.Request.URL.Path)
			//fmt.Println(c.Request.URL.RawPath)
			//fmt.Println(c.Request.URL.RawQuery)
			//fmt.Println(c.Request.URL.String())
			//c.Writer.WriteHeader(404)
			//_, _ = c.Writer.Write([]byte("page was not found1"))
			if !c.consumer.IsAutoAck {
				_ = c.RetryClient.Ack()
			}
			logLineF("路由不存在:%s", reqPath)
			return
		}
	} else {

	}
	//pathRoot := routeTree.getChildNode(c.Request.Path, )

}

/**
引擎处理
*/
func (dce *TaskEngine) Handle(req *dataRequest, retryClient kelleyRabbimqPool.RetryClientInterface, d *dataCenterConsumer) bool {
	c := dce.pool.Get().(*TaskContext)
	c.Request = req
	c.consumer = d
	c.RetryClient = retryClient
	//重试初始化当前加载的路由
	c.reset()

	//执行路由筛选及执行方法
	dce.routeHandle(c)
	//初始缓存池
	dce.pool.Put(c)
	return c.isReTry
}

/**
字符切片去重及去0值
*/
func sliceFilterString(input []string) []string {
	result := []string{}
	for _, e := range input {
		if len(e) > 0 {
			result = append(result, e)
		}
	}
	return result
}

func insertHandleSliceCopy(slice, insertion HandlersChain, index int) HandlersChain {
	result := make(HandlersChain, len(slice)+len(insertion))
	at := copy(result, slice[:index])
	at += copy(result[at:], insertion)
	copy(result[at:], slice[index:])
	return result
}
