package RabbitmqRoute

import (
	"fmt"
	"time"
)

var cstZone = time.FixedZone("CST", 8*3600)

func logLine(message string) {
	formatString := ""
	timeStamp := time.Now().In(cstZone)
	formatString = timeStamp.Format("2006-01-02 15:04:05")
	formatString = "_" + formatString + " : " + message
	fmt.Println(formatString)
}

func logLineF(message string, args ...interface{}) {
	formatString := ""
	timeStamp := time.Now().In(cstZone)
	formatString = timeStamp.Format("2006-01-02 15:04:05")
	formatString = "_" + formatString
	noticeMesg := fmt.Sprintf("=====%s======\n%+v\r\n", formatString, message)
	fmt.Println(noticeMesg+" args: %+v\n", args)
}
