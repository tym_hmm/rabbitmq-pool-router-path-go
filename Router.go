package RabbitmqRoute

import (
	"bytes"
	"fmt"
	"os"
	"path"
)

/**
数据统一回调处理
*/
type HandleFunc func(c *TaskContext)

/**
回调链
*/
type HandlersChain []HandleFunc

/**
记录当前路由信息
*/
type routerNode struct {
	path        string
	children    []*routerNode
	httpHandler HandlersChain
	index       int8
}

/**
添加子路由
*/
func (r *routerNode) addNode(path string, httpHandlers HandlersChain) {
	child := &routerNode{path: path, httpHandler: httpHandlers}
	r.children = append(r.children, child)
}

/**
构建路由数
*/
type RouterTree struct {
	root *routerNode
}

/**
根据根节点
*/
func (tree *RouterTree) getRoot() *routerNode {
	return tree.root
}

/**
获取路由表
*/
func (tree *RouterTree) getChildNode(path string, root *routerNode) *routerNode {
	if len(root.children) > 0 {
		for _, value := range root.children {
			if value.path == path {
				return value
			}
		}
	}
	return nil
}

type Router struct {
	Handlers HandlersChain
	engine   *TaskEngine
	basePath string
}

/**非中间件组**/
func (r *Router) Group(path string, handlers ...HandleFunc) *Router {
	return &Router{
		Handlers: r.combineHandlers(handlers),
		engine:   r.engine,
		basePath: r.calculateAbsolutePath(path),
	}
}

func (r *Router) UseMiddleware(handler ...HandleFunc) {
	r.Handlers = append(r.Handlers, handler...)
}
func (r *Router) AddRoute(path string, handler ...HandleFunc) {
	r.handlerFunc(path, handler)
}

func (r *Router) CreateRouter() {
	var buffer bytes.Buffer
	//buffer.WriteString(fmt.Sprintf("has http route handler :%d\n", len(r.engine.trees)))
	//for _, r1 := range r.engine.trees {
	//	buffer.WriteString(fmt.Sprintf("-[method] " + r1.method + "\n"))
	buffer.WriteString("=================rabbitMq Route Path================\n")
	if r.engine.trees.root != nil {
		for _, r2 := range r.engine.trees.root.children {
			fmtStr := fmt.Sprintf("--[path] %s, [handler] %T [handlerLen] %d", r2.path, r2.httpHandler, len(r2.httpHandler))
			buffer.WriteString(fmtStr + "\n")
		}
	}
	//}
	_, _ = fmt.Fprintln(os.Stdout, buffer.String())
	//logLineF("rabbitMq route %s \n", buffer.String())
}

func (r *Router) handlerFunc(path string, handler HandlersChain) {
	r.handle(path, handler)
}

func (r *Router) handle(path string, handler HandlersChain) {
	path = mergePath(r.basePath, path)
	handler = r.combineHandlers(handler)
	r.addRoute(path, handler)
}

func (r *Router) addRoute(path string, handler HandlersChain) {
	root := r.engine.trees.getRoot()
	root.addNode(path, handler)
}

func (r *Router) combineHandlers(handlers HandlersChain) HandlersChain {
	finalSize := len(r.Handlers) + len(handlers)
	if finalSize >= int(abortIndex) {
		panic("too many handlers")
	}
	mergedHandlers := make(HandlersChain, finalSize)
	copy(mergedHandlers, r.Handlers)
	copy(mergedHandlers[len(r.Handlers):], handlers)
	return mergedHandlers
}

func (r *Router) calculateAbsolutePath(relativePath string) string {
	return joinPaths(r.basePath, relativePath)
}
func mergePath(basePath, rpath string) string {
	if rpath == "" {
		return basePath
	}
	finalPath := path.Join(basePath, rpath)
	if rpath[len(rpath)-1] == '/' && finalPath[len(finalPath)-1] != '/' {
		return finalPath + "/"
	}
	return finalPath
}

func joinPaths(absolutePath, relativePath string) string {
	if relativePath == "" {
		return absolutePath
	}
	finalPath := path.Join(absolutePath, relativePath)
	if lastChar(relativePath) == '/' && lastChar(finalPath) != '/' {
		return finalPath + "/"
	}
	return finalPath
}

func lastChar(str string) uint8 {
	if str == "" {
		panic("The length of the string can't be 0")
	}
	return str[len(str)-1]
}
